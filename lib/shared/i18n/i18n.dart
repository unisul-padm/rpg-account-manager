import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_default.dart';

/*
*  Classe usada nos padrões do i18n internacionalização do flutter
*/
class GeneratedLocalizationsDelegate extends LocalizationsDelegate<Intl> {
  const GeneratedLocalizationsDelegate();

  List<Locale> get supportedLocales {
    return [Locale("en", "")];
  }

  LocaleListResolutionCallback listResolution(
      {Locale fallback, bool withCountry = true}) {
    return (List<Locale> locales, Iterable<Locale> supported) {
      if (locales == null || locales.isEmpty) {
        return fallback ?? supported.first;
      } else {
        return _resolve(locales.first, fallback, supported, withCountry);
      }
    };
  }

  LocaleResolutionCallback resolution(
      {Locale fallback, bool withCountry = true}) {
    return (Locale locale, Iterable<Locale> supported) {
      return _resolve(locale, fallback, supported, withCountry);
    };
  }

  @override
  Future<Intl> load(Locale locale) {
    final String lang = getLang(locale);
    if (lang != null) {
      Intl.current = const $en();
      return SynchronousFuture<Intl>(Intl.current);
    }
    Intl.current = const Intl();
    return SynchronousFuture<Intl>(Intl.current);
  }

  @override
  bool isSupported(Locale locale) => isSupportedLocale(locale);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;

  Locale _resolve(Locale locale, Locale fallback, Iterable<Locale> supported,
      bool withCountry) {
    if (locale == null || !isSupportedLocale(locale)) {
      return fallback ?? supported.first;
    }

    final Locale languageLocale = Locale(locale.languageCode, "");
    if (supported.contains(locale)) {
      return locale;
    } else if (supported.contains(languageLocale)) {
      return languageLocale;
    } else {
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    }
  }

  LocaleResolutionCallback localeResolutionCallback() {
    return (Locale locale, Iterable<Locale> supported) {
      return resolveLocale(locale, Locale("en", ""), supported);
    };
  }

  Locale resolveLocale(
      Locale locale, Locale fallback, Iterable<Locale> supported) {
    if (locale == null || !isSupportedLocale(locale)) {
      return fallback ?? supported.first;
    }

    final Locale languageLocale = Locale(locale.languageCode, "");
    if (supported.contains(locale)) {
      return locale;
    } else if (supported.contains(languageLocale)) {
      return languageLocale;
    } else {
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    }
  }

  bool isSupportedLocale(Locale locale) {
    if (locale != null) {
      for (Locale supportedLocale in Intl.delegate.supportedLocales) {
        // Language must always match both locales.
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}

String getLang(Locale l) => l == null
    ? null
    : l.countryCode != null && l.countryCode.isEmpty
        ? l.languageCode
        : l.toString();
