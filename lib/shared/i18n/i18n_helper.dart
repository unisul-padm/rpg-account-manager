import 'package:flutter/widgets.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_default.dart';

/*
*  Classe que inicializa o esquema de internacionalização no padrão i18n para flutter
*/
Intl translate(BuildContext context) => Intl.of(context);
