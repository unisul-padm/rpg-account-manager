import 'package:flutter/material.dart';
import 'package:rpg_account_manager/shared/i18n/i18n.dart';

class $en extends Intl {
  const $en();
}

/*
*  Classe usada nos padrões do i18n internacionalização do flutter
*/
class Intl implements WidgetsLocalizations {
  const Intl();

  static Intl current;

  static const GeneratedLocalizationsDelegate delegate =
      GeneratedLocalizationsDelegate();

  static Intl of(BuildContext context) => Localizations.of<Intl>(context, Intl);

  /*
  * Definição de palavras chave
  */
  @override
  TextDirection get textDirection => TextDirection.ltr;
  String get app_title => "RPG Account Manager";
  String get character => "Characters";
  String get delete => "Delete";
  String get edit => "Edit";
  String get orcish => "Orcish";
  String get drow => "Drow";
  String get draconic => "Draconic";
  String get dwarvish => "Dwarvish";
  String get halfling => "Halfling";
  String get elvish => "Elvish";
  String get human => "Human";
  String get name => "Name";
  String get className => "Class";
  String get description => "Description";
  String get gender => "Gender";
  String get race => "Race";
  String get no_image_selected => "No image selected";
  String get image_loaded => "Image loaded";
  String get required_field => "Required Field";
  String get male => "Male";
  String get female => "Female";
}
