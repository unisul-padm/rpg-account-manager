import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rpg_account_manager/commons/entities/character.dart';
import 'package:rpg_account_manager/commons/services/api/character_api_service.dart';
import 'package:rpg_account_manager/commons/services/screen/character_conversion_service.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_default.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_helper.dart';

class AddCharacterScreen extends StatefulWidget {
  Character character; // dados para carregar personagem existente
  final bool editable; // define se o é apenas vizualização
  AddCharacterScreen({this.character, this.editable});

  @override
  _AddCharacterScreenState createState() => _AddCharacterScreenState();
}

class _AddCharacterScreenState extends State<AddCharacterScreen> {
  TextEditingController _nameController;
  TextEditingController _descriptionController;
  TextEditingController _classController;
  String _gender = Intl.current.male;
  String _race = Intl.current.human;
  File _image;
  bool _loadingName = false;

  final _formKey = GlobalKey<FormState>();
  final _picker = ImagePicker();
  List<DropdownMenuItem> _races = [
    DropdownMenuItem(
      child: Text(Intl.current.human),
      value: Intl.current.human,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.elvish),
      value: Intl.current.elvish,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.halfling),
      value: Intl.current.halfling,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.dwarvish),
      value: Intl.current.dwarvish,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.draconic),
      value: Intl.current.draconic,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.drow),
      value: Intl.current.drow,
    ),
    DropdownMenuItem(
      child: Text(Intl.current.orcish),
      value: Intl.current.orcish,
    ),
  ];

  @override
  void initState() {
    // método inicializador da página, carrega vars e estados
    super.initState();
    _nameController = TextEditingController();
    _descriptionController = TextEditingController();
    _classController = TextEditingController();
    _loadingName = false;

    if (widget.character != null) {
      _nameController.text = widget.character.name;
      _classController.text = widget.character.className;
      _descriptionController.text = widget.character.description;
      _gender = widget.character.gender;
      _race = widget.character.race;
      if (widget.character.imagePath != null) {
        _image = File(widget.character.imagePath);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // método que desenha a tela
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context).app_title),
        actions: <Widget>[
          Visibility(
            visible: widget.editable,
            child: IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                this.saveCharacter();
              },
            ),
          ),
          Visibility(
            visible: !widget.editable,
            child: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                deleteCharacter();
              },
            ),
          ),
        ],
      ),
      body: generateScrollView(),
    );
  }

  generateScrollView() {
    //Desenha bloco principal da tela, usando o scrollView para evitar erros de overflow
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        generateImageRow(),
        Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              generateDropDown(),
              Visibility(
                visible: widget.editable,
                child: Row(children: <Widget>[
                  generateRadio(translate(context).male),
                  generateRadio(translate(context).female),
                ]),
              ),
              generateInput(translate(context).name, _nameController,
                  sufix: true),
              generateInput(translate(context).className, _classController),
              generateInput(
                  translate(context).description, _descriptionController,
                  multiline: true),
            ],
          ),
        )
      ],
    ));
  }

  generateImageRow() {
    // Desenha linha principal da view relacionada a imagem de perfil
    return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(16),
              child: Stack(children: <Widget>[
                Container(
                  height: 60,
                  width: 60,
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey[300],
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: _image == null
                              ? ExactAssetImage("lib/commons/assets/user.png")
                              : FileImage(_image))),
                ),
                Visibility(
                  visible: widget.editable,
                  child: Positioned(
                      top: 40,
                      left: 40,
                      child: Container(
                          height: 20,
                          width: 20,
                          child: FloatingActionButton(
                            child: Icon(
                              _image == null ? Icons.add : Icons.edit,
                              size: 10,
                            ),
                            onPressed: () {
                              _picker
                                  .getImage(source: ImageSource.gallery)
                                  .then((file) {
                                this.setState(() {
                                  _image = File(file.path);
                                });
                              });
                            },
                          ))),
                )
              ])),
          generateImageWarning(),
          generateInformativeData(),
        ]);
  }

  generateImageWarning() {
    // método para criar avisos  relacionados a imagem de perfil
    return Visibility(
      visible: widget.editable,
      child: Column(children: <Widget>[
        Text(_image == null
            ? translate(context).no_image_selected
            : translate(context).image_loaded),
      ]),
    );
  }

  generateInformativeData() {
    // método para desenhar dados do character (usado apenas no modo de visualização)
    return Visibility(
      visible: !widget.editable,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("${translate(context).race}: $_race"),
            Text("${translate(context).gender}: $_gender"),
          ]),
    );
  }

  generateDropDown() {
    // método para criar dropDown
    return Visibility(
      visible: widget.editable,
      child: Container(
        padding: EdgeInsets.all(8),
        child: DropdownButtonFormField(
          value: _race,
          items: _races,
          onChanged: widget.editable
              ? (value) {
                  this.setState(() {
                    this._race = value;
                  });
                }
              : null,
          hint: Text(translate(context).race),
          decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    style: BorderStyle.solid,
                    color: Color.fromRGBO(197, 206, 215, 1),
                  ),
                  borderRadius: BorderRadius.circular(8))),
        ),
      ),
    );
  }

  generateInput(String name, controller, {bool multiline, bool sufix}) {
    // método para evitar repetição de código na hora de desenhar inputs
    return Container(
        padding: EdgeInsets.all(8),
        child: TextFormField(
          validator: (text) {
            if (text == null || text == '' || text.length < 1) {
              return translate(context).required_field;
            }
            return null;
          },
          enabled: widget.editable,
          controller: controller,
          maxLines: multiline == true ? null : 1,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
              labelText: name,
              suffixIcon: Visibility(
                visible: sufix != null,
                child: !this._loadingName
                    ? IconButton(
                        icon: Icon(Icons.fiber_new),
                        onPressed: () {
                          this.setState(() {
                            this._loadingName = true;
                          });
                          CharacterAPIService()
                              .usingGetgenerateName(_race, _gender)
                              .then((value) {
                            this.setState(() {
                              this._loadingName = false;
                              _nameController.text = value;
                            });
                          });
                        })
                    : Padding(
                        padding: EdgeInsets.all(8),
                        child: CircularProgressIndicator()),
              ),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                    style: BorderStyle.solid,
                    color: Color.fromRGBO(197, 206, 215, 1),
                  ),
                  borderRadius: BorderRadius.circular(8))),
        ));
  }

  generateRadio(String name) {
    // método para evitar repetição de código na hora de desenhar radios
    return Column(children: <Widget>[
      Row(
        children: <Widget>[
          Radio(
            value: name,
            groupValue: _gender,
            onChanged: widget.editable
                ? (String value) {
                    setState(() {
                      _gender = value;
                    });
                  }
                : null,
          ),
          Text(name),
        ],
      )
    ]);
  }

  saveCharacter() async {
    // método que valida e salva o personagem no sharedPreferences chamando o serviço CharacterConvertionService
    if (this._formKey.currentState.validate()) {
      if (widget.character == null) widget.character = new Character();
      widget.character.name = _nameController.value.text;
      widget.character.className = _classController.value.text;
      widget.character.race = _race;
      widget.character.imagePath = _image?.path;
      widget.character.description = _descriptionController.value.text;
      widget.character.gender = _gender;
      CharacterConvertionService.saveCharacter(widget.character);
      Navigator.of(context).pop(CharacterConvertionService.getCharacters());
    }
  }

  deleteCharacter() {
    // método que chama serviço CharacterConvertionService para deletar personagem no sharedPreferences
    CharacterConvertionService.deleteCharacter(widget.character);
    Navigator.of(context).pop(CharacterConvertionService.getCharacters());
  }
}
