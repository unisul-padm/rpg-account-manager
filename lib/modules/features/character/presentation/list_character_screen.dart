import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rpg_account_manager/commons/entities/character.dart';
import 'package:rpg_account_manager/commons/services/screen/character_conversion_service.dart';
import 'package:rpg_account_manager/modules/features/character/presentation/add_character_screen.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_helper.dart';

class ListCharacterScreen extends StatefulWidget {
  @override
  _ListCharacterScreenState createState() => _ListCharacterScreenState();
}

class _ListCharacterScreenState extends State<ListCharacterScreen> {
  @override
  Widget build(BuildContext context) {
    // método que desenha a tela
    return Scaffold(
      appBar: AppBar(
        title: Text(translate(context).app_title),
        actions: <Widget>[],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(8),
            child: Text(
              "${translate(context).character}:",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          Expanded(child: characterBuilder())
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      AddCharacterScreen(editable: true)))
              .then((chars) {
            this.setState(() {});
          });
        },
      ),
    );
  }

  characterBuilder() {
    //método que desenha a lista de personagens assim que a busca no cache termina
    return FutureBuilder(
        future: CharacterConvertionService.getCharacters(),
        builder: (context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          } else {
            return Container(
                child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      Character char = snapshot.data[index];
                      return Card(
                          child: ListTile(
                        leading: Container(
                          height: 50,
                          width: 50,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.grey[300],
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: char.imagePath == null
                                      ? ExactAssetImage(
                                          "lib/commons/assets/user.png")
                                      : FileImage(File(char.imagePath)))),
                        ),
                        title: Text(char.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18)),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(char.className),
                            Text(char.race),
                            Text(char.gender),
                          ],
                        ),
                        trailing: buildPopMenu(char),
                        onTap: () {
                          viewCharacter(char);
                        },
                      ));
                    }));
          }
        });
  }

  buildPopMenu(Character char) {
    // método que builda o menu de opção dos personagens
    return PopupMenuButton(
        onSelected: (result) {},
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                child: FlatButton.icon(
                    label: Text(translate(context).edit),
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      editCharacter(char);
                    }),
              ),
              PopupMenuItem<String>(
                child: FlatButton.icon(
                    label: Text(translate(context).delete),
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      deleteCharacter(char);
                    }),
              )
            ]);
  }

  viewCharacter(Character char) {
    // método para visualização de personagem, sem opção de edição
    Navigator.of(context)
        .push(MaterialPageRoute(
            builder: (BuildContext context) =>
                AddCharacterScreen(character: char, editable: false)))
        .then((chars) {
      this.setState(() {});
    });
  }

  editCharacter(Character char) {
    // método para edição de personagem
    Navigator.of(context).pop();
    Navigator.of(context)
        .push(MaterialPageRoute(
            builder: (BuildContext context) =>
                AddCharacterScreen(character: char, editable: true)))
        .then((chars) {
      this.setState(() {});
    });
  }

  deleteCharacter(Character char) {
    // método para deletar personagem
    Navigator.of(context).pop();
    CharacterConvertionService.deleteCharacter(char);
    this.setState(() {});
  }
}
