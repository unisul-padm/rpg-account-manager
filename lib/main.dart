import 'package:flutter/material.dart';
import 'package:rpg_account_manager/modules/features/character/presentation/add_character_screen.dart';
import 'package:rpg_account_manager/modules/features/character/presentation/list_character_screen.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_default.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rpg_account_manager/shared/i18n/i18n_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (context) => translate(context).app_title,
      localizationsDelegates: [
        Intl.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: Intl.delegate.supportedLocales,
      localeResolutionCallback: Intl.delegate.localeResolutionCallback(),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => ListCharacterScreen(),
        '/add': (context) => AddCharacterScreen(),
      },
    );
  }
}
