import 'package:rpg_account_manager/commons/entities/character.dart';

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

/* 
*Classe service com métodos estaticos para delete, create, edit e list de personagens. 
*Esta classe faz uso do SharedPreferences, que é um plugin para fazer uso de cache no lado do cliente. 
*/
class CharacterConvertionService {
  static final String _characterKey = "CHARACTERS";
  static final String _characterIdKey = "IDCHARACTERS";

  static saveCharacter(Character character) async {
    // método que salva o personagem no SharedPreferences
    SharedPreferences shared = await SharedPreferences.getInstance();
    String rawChars = shared.getString(_characterKey);
    List<Character> chars = List();
    if (rawChars != null && rawChars.isNotEmpty) {
      List<dynamic> items = json.decode(shared.getString(_characterKey));
      items.forEach((element) {
        chars.add(Character.fromJson(element));
      });
    }
    if (character.id != null) {
      chars.removeWhere((element) => element.id == character.id);
    } else {
      character.id = await generateId();
    }
    chars.add(character);
    shared.setString(_characterKey, json.encode(chars));
    print(chars);
  }

  static deleteCharacter(Character character) async {
    // método que faz uso do SharedPreferences e deleta o personagem selecionado
    SharedPreferences shared = await SharedPreferences.getInstance();
    String rawChars = shared.getString(_characterKey);
    List<Character> chars = List();
    if (rawChars != null && rawChars.isNotEmpty) {
      List<dynamic> items = json.decode(shared.getString(_characterKey));
      items.forEach((element) {
        chars.add(Character.fromJson(element));
      });
    }
    chars.removeWhere((element) => element.id == character.id);
    shared.setString(_characterKey, json.encode(chars));
  }

  static Future<List<Character>> getCharacters() async {
    // método para listar personagens salvos em cache
    SharedPreferences shared = await SharedPreferences.getInstance();
    String rawChars = shared.getString(_characterKey);
    List<Character> chars = List();
    if (rawChars != null && rawChars.isNotEmpty) {
      List<dynamic> items = json.decode(shared.getString(_characterKey));
      items.forEach((element) {
        chars.add(Character.fromJson(element));
      });
    }
    return chars;
  }

  static Future<int> generateId() async {
    // método para gerar um id para os novos personagens e controlar o id geral
    SharedPreferences shared = await SharedPreferences.getInstance();
    int id = shared.getInt(_characterIdKey);
    if (id == null) id = 0;
    shared.setInt(_characterIdKey, ++id);
    return id;
  }
}
