import 'dart:async';
import 'package:http/http.dart' as http;

/*
*Classe service que faz o lado de conexão com a api de geração de nomes voltados para RPG
*/
class CharacterAPIService {
  //método que chama o serviço de geração de nomes
  Future<String> usingGetgenerateName(String race, String gender) async {
    String url =
        "https://donjon.bin.sh/name/rpc-name.fcgi?type=$race+$gender&n=1";
    return http
        .get(
      url,
    )
        .then((response) {
      return response.body;
    }).catchError((err) {
      throw err;
    });
  }
}
