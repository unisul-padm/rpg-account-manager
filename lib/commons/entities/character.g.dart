// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Character _$CharacterFromJson(Map<String, dynamic> json) {
  return Character(
    name: json['name'] as String,
    description: json['description'] as String,
    className: json['className'] as String,
    gender: json['gender'] as String,
    imagePath: json['imagePath'] as String,
    race: json['race'] as String,
  )..id = json['id'] as int;
}

Map<String, dynamic> _$CharacterToJson(Character instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'gender': instance.gender,
      'race': instance.race,
      'className': instance.className,
      'imagePath': instance.imagePath,
    };
