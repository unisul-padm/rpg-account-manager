import 'package:json_annotation/json_annotation.dart';

part 'character.g.dart';

/*
*Classe entitida de personagem, faz uso de JsonSerializable para geração do modelo em json
*/

@JsonSerializable()
class Character {
  int id;
  String name;
  String description;
  String gender;
  String race;
  String className;
  String imagePath;

  Character(
      {this.name,
      this.description,
      this.className,
      this.gender,
      this.imagePath,
      this.race});

  factory Character.fromJson(Map<String, dynamic> json) =>
      _$CharacterFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterToJson(this);
}
